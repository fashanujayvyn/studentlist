package sheridan.Jay.student;

import java.util.Scanner;
 

/**
 * This class is a simple example of creating arrays of objects
 *
 * @author Paul Bonenfant
 */
public class StudentList {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
            
        String name = args[0];
String id = args[1];


Student student = new Student(name,id);

System.out.printf("The students name is %s and their id is %s",student.getName(),student.getId());
    }

}

