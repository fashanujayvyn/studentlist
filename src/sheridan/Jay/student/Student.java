package sheridan.Jay.student;

/**
 * This class represents students in our application
 *
 * @author Paul Bonenfant
 */
public class Student {
    private String id;
    private String name;
private String program;
    public Student(String name, String id) {//constructor
        this.name = name;
        this.id=id;
    }

    public String getName() {// accesor method
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getId(){
        return id;
    }
    
    public String getprogram(){
     return program;   
        
    }
    public void setprogram(String program){
     this.program = program;   
        
    }

}

